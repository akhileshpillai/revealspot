FROM python:2.7.8
MAINTAINER Akhilesh Pillai 
EXPOSE 22 
EXPOSE 8000
 
RUN mkdir -p /usr/src/app
COPY requirements.txt /usr/src/requirements.txt
COPY .env /usr/src/app/.env

RUN pip install -U pip
 
WORKDIR /usr/src/python
RUN pip install -r /usr/src/requirements.txt


#ENV DEBUG on 
#ENV DATABASE_URL postgres://postgres@db/postgres
#ENV SECRET_KEY 'tob4=1px1r&lwfn%6_@7uedfk#ln9t^um&xgbx1ygtouph#2z8'
#ENV DJANGO_SETTINGS_MODULE reveal.settings

WORKDIR /usr/src/app
RUN ls -l  

WORKDIR /usr/src/app
CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000" ]
