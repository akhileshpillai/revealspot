import hashlib
from django.shortcuts import render
from django.views.generic import ListView
from postvideo.models import PostVideo
from postvideo.forms import PostVideoForm
from django.core.urlresolvers import reverse
from django.views.generic import CreateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.cache import cache
# Create your views here.
# I think this index function can be moved to a seperate app later.
def index(request):
        return render_to_response("index.html",RequestContext(request))

class LoggedInMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoggedInMixin, self).dispatch(*args, **kwargs)

class PostVideoView(LoggedInMixin,ListView):
    model = PostVideo
    template_name = 'video_list.html'
    def get_queryset(self):
        return PostVideo.objects.filter(owner=self.request.user)


class CreateVideoView(CreateView):
    model = PostVideo
    template_name = 'edit_video.html'
    def get_success_url(self):
        return reverse('video-list')

class PostVideoFormView(LoggedInMixin, CreateView):
    model = PostVideo
    template_name = 'edit_video.html'
    form_class = PostVideoForm
    def form_valid(self,form):
        data = form.save(commit=False)
        data.owner = self.request.user
        data.save()
        return HttpResponseRedirect(reverse('video-list'))
    def get_success_url(self):
        return reverse('video-list')

class SearchView(LoggedInMixin, CreateView):
    model = PostVideo
    template_name = 'searchtag.html'
    def post(self, request):
        listobjects = []
        tg = request.POST['tag']
        tags = tg.split(',')
        tags.sort
        mkey = hashlib.sha256(','.join(tags)).hexdigest()
        print "mkey Mkey monkey ", mkey
        video_obj = cache.get(mkey)
        
        if not video_obj:  
            print "Cache Miss so going to database"
            sv = PostVideo.objects.filter(tag__overlap=tags)  # Query the database
            if sv:                                            # if results are available 
                for i in sv:                                  # create a list of objects
                    listobjects.append(i)
            cache.set(mkey,listobjects,300)                   # populate the cache with this list of objects
            video_obj = listobjects                                 

        paginator = Paginator(video_obj, 25) # Show 25 contacts per page
        page = request.GET.get('page')
        try:
            vpp = paginator.page(page)
        except PageNotAnInteger:
        #    If page is not an integer, deliver first page.
            vpp = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            vpp = paginator.page(paginator.num_pages)

        return render(request, self.template_name, {'searchV': vpp,'tags':tags})
