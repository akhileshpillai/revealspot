from django.forms import ModelForm
from postvideo.models import PostVideo

class PostVideoForm(ModelForm):
    class Meta:
        model = PostVideo
        fields = ["video_name", "tag"]

