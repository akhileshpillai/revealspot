from django.contrib.auth.models import User
from allauth.account.models import EmailAddress
from allauth.socialaccount.models import SocialAccount
from django.db import models
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.core.cache import cache
from django.contrib.auth.models import User
from time import strftime
from django.contrib.postgres.fields import ArrayField
import hashlib
# Create your models here.

def content_file_name(instance, filename):
    part_name = "reviewvideos/" 
    filename = strftime("%y%m%d%H%M%S") + filename
    owner =  str(instance.owner) 
    return '/'.join([part_name, owner, filename])

class PostVideo(models.Model):
    video_name = models.FileField(upload_to=content_file_name,) 
    tag = ArrayField(models.CharField(max_length=255,))
    owner = models.CharField(max_length=255,) 

    def __str__(self):
        return ' '.join([self.video_name, self.tag,])

# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
 
    def __unicode__(self):
        return "{}'s profile".format(self.user.username)
 
    class Meta:
        db_table = 'user_profile'
 
    def account_verified(self):
        if self.user.is_authenticated:
            result = EmailAddress.objects.filter(email=self.user.email)
            if len(result):
                return result[0].verified
        return False

    def profile_image_url(self):
        print "inside profile_image_url"
        fb_uid = SocialAccount.objects.filter(user_id=self.user.id, provider='facebook')
    
        if len(fb_uid):
            return  "http://graph.facebook.com/{}/picture?width=40&height=40".format(fb_uid[0].uid)

        return "http://www.gravatar.com/avatar/{}?s=40".format(hashlib.md5(self.user.email).hexdigest())
 
User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

