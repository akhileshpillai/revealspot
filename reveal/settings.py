import sys, os
import commands
print >> sys.stderr, commands.getoutput('pip freeze')
print >> sys.stderr, sys.path
import environ
root = environ.Path("manage.py") - 1
print root.root
environ.Env.read_env(root.root + "/" +".env")
#environ.Env.read_env() # reading .env file

"""
Django settings for reveal project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
env = environ.Env(DEBUG=(bool, True),) # set default values and casting


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY= 'tob4=1px1r&lwfn%6_@7uedfk#ln9t^um&xgbx1ygtouph#2z8'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env('DEBUG') 

TEMPLATE_DEBUG = DEBUG 

ALLOWED_HOSTS = []

TEMPLATE_CONTEXT_PROCESSORS = (
    # Required by allauth template tags
    'django.contrib.auth.context_processors.auth',
    "django.core.context_processors.request",
    # allauth specific context processors
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount",
)

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)



# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'storages',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    # ... include the providers you want to enable:
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'postvideo',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'reveal.urls'

WSGI_APPLICATION = 'reveal.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default' : env.db()
}

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#    }
#}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

CACHES = {
    'default': env.cache()
}

# s3 storage related settings
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
AWS_ACCESS_KEY_ID='AKIAJ7KMO7BACKCXJXTA'
AWS_SECRET_ACCESS_KEY='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'
AWS_STORAGE_BUCKET_NAME='revealspot'
AWS_S3_CUSTOM_DOMAIN= '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
STATIC_URL="http://%s/" % AWS_S3_CUSTOM_DOMAIN
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'


TEMPLATE_DIRS = ( 
        'reveal/templates',
        )

SITE_ID = 1


# auth and allauth settings
LOGIN_REDIRECT_URL = '/'
SOCIALACCOUNT_QUERY_EMAIL = True
ACCOUNT_UNIQUE_EMAIL = False
ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_CONFIRM_EMAIL_ON_GET = True

SOCIALACCOUNT_PROVIDERS = {
            'facebook': {
                        'SCOPE': ['email', 'publish_stream'],
                                'METHOD': 'oauth2'  # instead of 'oauth2'
             },
                'google': {
                             'SCOPE': ['https://www.googleapis.com/auth/userinfo.profile','email'],
                                      'AUTH_PARAMS': { 'access_type': 'online' } ,
                                      'REDIRECT_URL': 'http://localhost:8000/accounts/google/login/callback/',
                }
}

