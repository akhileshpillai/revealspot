from django.conf.urls import patterns, include, url
from django.contrib import admin
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import postvideo.views
#admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^videos/', postvideo.views.PostVideoView.as_view(),name='video-list'),
    url(r'^upload/', postvideo.views.PostVideoFormView.as_view(),name='newvideo'),
    url(r'^postV/', postvideo.views.SearchView.as_view(), name='searchVideo'),
    url(r'^accounts/login/$', 'postvideo.views.index'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout',{'next_page': '/'}),
    url(r'^$', 'postvideo.views.index',name='home'),
)

#urlpatterns += staticfiles_urlpatterns()
