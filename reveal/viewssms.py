from django.shortcuts import render_to_response
from django_twilio.decorators import twilio_view
from twilio.twiml import Response
from django_twilio.models import Caller
from onsmsreceive.models import SmsMessage
from onsmsreceive.models import SmsDailyTotal
from phonenumber_field.modelfields import PhoneNumberField
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from datetime import datetime 
@twilio_view
def sms(request):
         print "In the sms function view"
         msg = request.POST.get('Body', '')
         smsfrom = request.POST.get('From')
         for k,v in request.POST.items():
         	print "Parameter :%s Value: %s" % (k,v) 
         
         r = Response()
         datestamp = datetime.date(datetime(2014, 5, 1))       
	 try:
       	 	sender = Caller.objects.get(phone_number=smsfrom)
    	 except Caller.DoesNotExist:
        	print "Caller %s does not exist" % (smsfrom)
         sms=SmsMessage(sender=smsfrom,callerid=sender, message=msg,datestamp=datestamp)
         sms.save()
         
         
         r.message(msg)
         return r
# Create your views here.

@login_required
def listing(request):
    sms_list = SmsMessage.objects.all().order_by('-timestamp')
    paginator = Paginator(sms_list, 25) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        smsmsg = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        smsmsg = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        smsmsg = paginator.page(paginator.num_pages)

    return render_to_response('list.html', {"smsmsg": smsmsg})

@login_required
def dailyCheckin(request):
    checkpoints={}
    smsCheckpoints = SmsMessage.objects.filter(datestamp = datetime.today().date()).order_by('callerid__phone_number','timestamp')
    for i in smsCheckpoints:
        if i.callerid in checkpoints.keys():
            checkpoints[i.callerid].append(i.timestamp) 
        else:
            checkpoints[i.callerid] = [i.timestamp]
    
    return render_to_response('dailycheckin.html', {"checkpoints":checkpoints})

@login_required
def dailytotal(request):
    daily_total = SmsDailyTotal.objects.all().order_by('-datestamp')
    print daily_total
    paginator = Paginator(daily_total, 25) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        totalhours = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        totalhours = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        totalhours = paginator.page(paginator.num_pages)

    return render_to_response('dailytotal.html', {"totaldaily": totalhours})

